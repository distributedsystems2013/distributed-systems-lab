package test;

import cli.Shell;
import cli.TestInputStream;
import cli.TestOutputStream;
import client.IClientCli;
import message.Response;
import message.response.BuyResponse;
import message.response.LoginResponse;
import message.response.MessageResponse;
import proxy.IProxyCli;
import server.IFileServerCli;
import test.model.LoadTestConfig;
import util.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

public class LoadTest {

    static final int SUBSCRIPTIONS_PER_MINUTE = 120;

    static final Logger LOG = Logger.getLogger(LoadTest.class.getName());
    static final String LSEP = System.getProperty("line.separator");
    static final String FSEP = System.getProperty("file.separator");

    LoadTestConfig config;
    File tmpFolder = new File(System.getProperty("java.io.tmpdir"));
    File testFolder = new File(tmpFolder, "tuwien.dslab.lab2.loadtest.fileserver");
    List<String> filenames = Collections.synchronizedList(new ArrayList<String>());

    static ComponentFactory componentFactory = new ComponentFactory();
    IProxyCli proxy;
    IFileServerCli server;
    IClientCli subscriberClient;
    List<IClientCli> clients = new ArrayList<IClientCli>();

    static AtomicInteger uploadCount = new AtomicInteger(0);
    static AtomicInteger overwriteCount = new AtomicInteger(0);
    static AtomicInteger downloadCount = new AtomicInteger(0);
    static AtomicInteger timeoutCount = new AtomicInteger(0);

    public static void main(String... args) throws Exception {
        new LoadTest().before().loadTest();
    }

    public LoadTest before() throws Exception {
        LOG.info("loadTest started");

        deleteTestDir();
        testFolder.mkdir();
        LOG.info("test folders created: " + testFolder.getAbsolutePath());

        config = new LoadTestConfig(new ConfigExt("loadtest"));
        LOG.info("config loaded");

        initProxy();
        createFileserver(0);
        createFileserver(1);
        createFileserver(2);
        Thread.sleep(Util.WAIT_FOR_COMPONENT_STARTUP);
        for (int i=0; i<config.getClients(); i++) {
            clients.add(createClient(i));
        }
        subscriberClient = createClient(config.getClients()+100);
        Thread.sleep(Util.WAIT_FOR_COMPONENT_STARTUP);
        LOG.info("components created");

        return this;
    }

    public void after() {
        try { proxy.exit();} catch (IOException e) {e.printStackTrace();}
        try { server.exit();} catch (IOException e) {e.printStackTrace();}
        for (int i=0; i<clients.size(); i++) {
            try { clients.get(i).exit(); } catch (IOException e) {e.printStackTrace();}
        }
        LOG.info("loadTest finished");
    }

    public LoadTest loadTest() throws IOException, InterruptedException {
        LOG.info("login and buy for " + (clients.size()+1) + " clients...");
        // login with all clients
        LoginResponse loginResponse;
        Response buyResponse;
        for(int i=0; i<clients.size(); i++) {
            LOG.info("login client " + i);
            loginResponse = clients.get(i).login("alice", "12345");
            if(loginResponse.getType() == LoginResponse.Type.WRONG_CREDENTIALS) {
                LOG.info("login failed");
                return this;
            }
        }
        buyResponse = clients.get(0).buy(10000 * clients.size());
        if(buyResponse instanceof MessageResponse) {
            LOG.info("buy failed");
            LOG.info(((MessageResponse) buyResponse).getMessage());
            return this;
        }
        loginResponse = subscriberClient.login("bill", "23456");
        if(loginResponse.getType() == LoginResponse.Type.WRONG_CREDENTIALS) {
            LOG.info("login failed");
            return this;
        }
        buyResponse = subscriberClient.buy(100000);
        if(buyResponse instanceof MessageResponse) {
            LOG.info("buy failed");
            LOG.info(((MessageResponse) buyResponse).getMessage());
            return this;
        }
        Thread.sleep(Util.WAIT_FOR_COMPONENT_STARTUP);
        LOG.info("login and buy finished");

        // do uploading and downloading
        for(int i=0; i<clients.size(); i++) {
            final IClientCli client = clients.get(i);
            final int index = i;

            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    synchronized (client) {
                        try {
                            upload(client, index);
                        } catch(SocketTimeoutException e) {
                            timeoutCount.getAndIncrement();
                        } catch (IOException e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
                        System.out.println("upload: " + uploadCount + ", overwrite: " + overwriteCount + ", download: " + downloadCount + ", timeouts: " + timeoutCount);
                    }
                }
            }, 0, (long) 60000.0 / config.getUploadsPerMin());

            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    synchronized (client) {
                        try {
                            Response r = download(client, index);
                            if(r instanceof MessageResponse)
                                LOG.info(((MessageResponse) r).getMessage());
                        } catch(SocketTimeoutException e) {
                            timeoutCount.getAndIncrement();
                        } catch (IOException e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
                        System.out.println("upload: " + uploadCount + ", overwrite: " + overwriteCount + ", download: " + downloadCount + ", timeouts: " + timeoutCount);
                    }
                }
            }, 0, (long) 60000.0 / config.getDownloadsPerMin());
        }

        // subscriber test
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                synchronized (subscriberClient) {
                    try {
                        subscribe(subscriberClient);
                    } catch (IOException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
            }
        }, 0, (long) 60000.0 / SUBSCRIPTIONS_PER_MINUTE);

        return this;
    }

    private Response upload(IClientCli client, int index) throws IOException {
        String filename;
        Response response;
        int size = filenames.size();
        if(Math.random() < config.getOverwriteRatio() && size > 0) {
            // overwrite existing file
            int selected = (int) (Math.random() * size);
            filename = filenames.get(selected);
            createNewFile(index, filename);
            response = client.upload(filename);
            overwriteCount.getAndIncrement();
        } else {
            // upload new file
            filename = createNewFile(index).getName();
            response = client.upload(filename);
            filenames.add(filename);
            uploadCount.getAndIncrement();
        }
        return response;
    }

    private Response download(IClientCli client, int index) throws IOException {
        Response response = null;
        int size = filenames.size();
        if(size > 0) {
            int selected = (int) (Math.random() * size);
            response = client.download(filenames.get(selected));
            downloadCount.getAndIncrement();
        }
        return response;
    }

    private File createNewFile(int clientIndex) throws IOException {
        String filename = clientIndex+"_"+new Date().getTime();
        return createNewFile(clientIndex, filename);
    }

    private File createNewFile(int clientIndex, String filename) throws IOException {
        File file = new File(testFolder.getAbsolutePath()+FSEP+"client"+clientIndex+FSEP+filename);
        file.createNewFile();
        byte[] b = new byte[config.getFileSizeKB() * 1024];
        new Random().nextBytes(b);
        FileOutputStream fos = new FileOutputStream(file);
        fos.write(b);
        fos.close();
        return file;
    }

    private void subscribe(IClientCli subscriber) throws IOException {
        int size = filenames.size();
        if(size > 0) {
            int selected = (int) (Math.random() * size);
            String filename = filenames.get(selected);
            LOG.info("subscribe to: " + filename);
            subscriber.subscribe(filename, 1);
        }
    }

    public void initProxy() throws Exception {
        TestInputStream proxyIn = new TestInputStream();
        proxyIn.addLine("12345"); // RSA key password
        proxy = componentFactory.startProxy(new Config("proxy"), new Shell("proxy", new TestOutputStream(System.out), proxyIn));
    }

    public void createFileserver(int i) throws Exception {
        String dir = testFolder.getAbsolutePath()+FSEP+"fs"+i;
        new File(dir).mkdir();
        Map<String, String> configEntries = new HashMap<String, String>();
        configEntries.put("fileserver.alive","1000");
        configEntries.put("fileserver.dir",dir);
        configEntries.put("tcp.port",""+(14892+i));
        configEntries.put("proxy.host","localhost");
        configEntries.put("proxy.udp.port","14891");
        configEntries.put("hmac.key","keys/hmac.key");
        server = componentFactory.startFileServer(new ConfigMap(configEntries), new Shell("fs"+i, new TestOutputStream(System.out), new TestInputStream()));
    }

    public IClientCli createClient(int i) throws Exception {
        String dir = testFolder.getAbsolutePath()+FSEP+"client" + i;
        new File(dir).mkdir();
        Map<String, String> configEntries = new HashMap<String, String>();
        configEntries.put("download.dir",dir);
        configEntries.put("proxy.host","localhost");
        configEntries.put("proxy.tcp.port","14890");
        configEntries.put("keys.dir","keys");
        configEntries.put("proxy.key", "keys/proxy.pub.pem");
        IClientCli client = componentFactory.startClient(new ConfigMap(configEntries), new Shell("client"+i, new TestOutputStream(System.out), new TestInputStream()));
        return client;
    }

    private void deleteTestDir() {
        if(testFolder.exists()) {
            deleteFile(testFolder);
        }
    }

    private void deleteFile(File file) {
        if(file.isFile()) {
            file.delete();
        } else {
            for(File f: file.listFiles()) {
                deleteFile(f);
            }
            file.delete();
        }
    }
}

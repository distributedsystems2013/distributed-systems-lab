package test.model;

import exception.ConfigException;
import util.ConfigExt;

import java.io.File;

public class LoadTestConfig {

    private int clients;
    private int uploadsPerMin;
    private int downloadsPerMin;
    private int fileSizeKB;
    private float overwriteRatio;

    public LoadTestConfig(ConfigExt config) throws ConfigException {
        clients = config.getInt("clients");
        uploadsPerMin = config.getInt("uploadsPerMin");
        downloadsPerMin = config.getInt("downloadsPerMin");
        fileSizeKB = config.getInt("fileSizeKB");
        overwriteRatio = config.getFloat("overwriteRatio");

        if(clients < 1) {
            throw new ConfigException("clients needs to be at least 1");
        }
        if(uploadsPerMin < 0) {
            throw new ConfigException("clients needs must be a positive integer");
        }
        if(downloadsPerMin < 0) {
            throw new ConfigException("downloadsPerMin must be a positive integer");
        }
        if(fileSizeKB < 0) {
            throw new ConfigException("fileSizeKB must be a positive integer");
        }
        if(overwriteRatio < 0.0 || overwriteRatio > 1.0) {
            throw new ConfigException("overwriteRatio can only be between 0.0 and 1.0");
        }
    }

    public int getClients() {
        return clients;
    }

    public int getUploadsPerMin() {
        return uploadsPerMin;
    }

    public int getDownloadsPerMin() {
        return downloadsPerMin;
    }

    public int getFileSizeKB() {
        return fileSizeKB;
    }

    public float getOverwriteRatio() {
        return overwriteRatio;
    }
}

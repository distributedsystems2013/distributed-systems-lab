package client;

import cli.Command;
import cli.Shell;
import message.Response;
import message.request.*;
import message.response.DownloadFileResponse;
import message.response.DownloadTicketResponse;
import message.response.LoginResponse;
import message.response.MessageResponse;
import model.DownloadTicket;
import model.CallbackInfo;
import org.bouncycastle.util.encoders.Base64;
import shared.IManagement;
import shared.ObjMgr;
import shared.RSAService;
import shared.StorageService;
import shared.secured.*;
import util.*;

import javax.crypto.SecretKey;
import java.io.*;
import java.net.Socket;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClientCli implements IClientCli {

    private static final Logger log = Logger.getLogger(ClientCli.class.getName());

    private String downloadDir;
    private Socket sock;
    private Shell sh;
    private String ctx;
    private IManagement management;
    private Config clientConfig;
    private String username;
    private AesSession aesSession;

    private static final String RMI_CONFIGFILE = "mc";
    private static final String CLIENT_CONFIGFILE = "client";
    private static final String CLIENT_KEYS_DIR = "keys.dir";
    private static final String PROXY_PUBLIC_FILENAME = "proxy.pub.pem";

    public static void main(String[] args) {
        Shell sh = new Shell("clientcli", System.out, System.in);
        new ClientCli(new Config("client"), sh);
        sh.run();
    }

    public ClientCli(Config conf, Shell sh) {
        clientConfig = conf;
        downloadDir = conf.getString("download.dir");
        String hostname = conf.getString("proxy.host");
        int port = conf.getInt("proxy.tcp.port");
        ctx = "client" + conf.toString();
        try {
            sock = new Socket(hostname, port);
            // TCP has 2h timeout, but we dont want to wait that long,
            // if the server doesnt answer in 2sec, throw a readtimeout
            // NEW: do not use timeout on client
            //sock.setSoTimeout(2000);
        } catch (IOException e) {
            try {
                sh.writeLine("Proxy Server not online !");
            } catch (IOException e1) {
                // can not write to shell, just close
                return;
            }
            return;
        }
        ObjMgr.getInstance().addService(ctx, "storageService", new StorageService());
        try {
            ((StorageService) ObjMgr.getInstance().getService(ctx, "storageService")).setStoragePath(downloadDir);
        } catch (IOException e) {
            log.severe("couldn't init storage");
            return;
        }
        try {
            Registry registry = LocateRegistry.getRegistry(RMIHelper.getHostname(RMI_CONFIGFILE), RMIHelper.getRMIPort(RMI_CONFIGFILE));
            management = (IManagement) registry.lookup(RMIHelper.getServiceName(RMI_CONFIGFILE));
        } catch (NotBoundException e) {
            log.log(Level.WARNING, "RMI Registry not bound");
        } catch (RemoteException e) {
            log.log(Level.WARNING, "Something failed while initializing RMI");
        }
        try {
            sh.writeLine("Client connected!");
        } catch (IOException e) {
            log.severe("shell broken, closing!");
            return;
        }
        this.sh = sh;
        sh.register(this);
    }

    public void cleanup() {
        if (sock != null && !sock.isClosed())
            try {
                sock.close();
            } catch (IOException e) {
                // cleanup shutdown
            }
        if (sh != null)
            sh.close();
    }

//    @Override
    //@Command
    @Deprecated
    public LoginResponse loginOld(String username, String password) throws IOException {
        LoginRequest lreq = new LoginRequest(username, password);
        LoginResponse loginResponse =  SocketUtils.sendAndReceiveObject(sock, lreq, LoginResponse.class);
        if(loginResponse.getType().equals(LoginResponse.Type.SUCCESS)) {
            this.username = username;
        } else {
            this.username = null;
        }
        return loginResponse;
    }

    @Override
    @Command
    public LoginResponse login(String username, String password) throws IOException {
        SecureRandom secureRandom = new SecureRandom();
        RSAService rsaService = new RSAService(clientConfig, username, password);
        byte[] clientChallange = new byte[32];
        secureRandom.nextBytes(clientChallange);
        SecLoginReq secLoginReq = new SecLoginReq(username, new String(Base64.encode(clientChallange)));
        RSAMessage lreq = new RSAMessage(rsaService.encryptMessage("proxy", secLoginReq));
        RSAMessage loginResponse =  SocketUtils.sendAndReceiveObject(sock, lreq, RSAMessage.class);
        Object o = rsaService.decryptMessage(loginResponse.getEncryptedMessage());
        if(!(o instanceof SecLoginResp))
            throw new IOException("received wrong message type");
        SecLoginResp response = (SecLoginResp) o;
        if(!Arrays.equals(Base64.decode(response.getClientChallenge()), clientChallange))
            throw new IOException("got invalid client challenge");
        aesSession = new AesSession((SecretKey) Base64Util.stringToObject(response.getSecretKey()), Base64.decode(response.getIv()));
        this.username = username;
        if(SocketUtils.sendAndReceiveObjectEnc(sock, new AesSyn(response.getProxyChallenge()), AesAck.class, aesSession) == null)
            throw new IOException("aes handshake failed");
        return new LoginResponse(LoginResponse.Type.SUCCESS);
    }

    @Override
    @Command
    public Response credits() throws IOException {
        CreditsRequest creq = new CreditsRequest();
        return SocketUtils.sendAndReceiveObjectEnc(sock, creq, Response.class, aesSession);
    }

    @Override
    @Command
    public Response buy(long credits) throws IOException {
        BuyRequest breq = new BuyRequest(credits);
        return SocketUtils.sendAndReceiveObjectEnc(sock, breq, Response.class, aesSession);
    }

    @Override
    @Command
    public Response list() throws IOException {
        ListRequest lreq = new ListRequest();
        return SocketUtils.sendAndReceiveObjectEnc(sock, lreq, Response.class, aesSession);
    }

    @Override
    @Command
    public Response download(String filename) throws IOException {
        ((StorageService) ObjMgr.getInstance().getService(ctx, "storageService")).deleteFile(filename);
        DownloadTicketRequest dtreq = new DownloadTicketRequest(filename);
        Response resp = SocketUtils.sendAndReceiveObjectEnc(sock, dtreq, Response.class, aesSession);
        if (resp instanceof MessageResponse) // not enough credits, did not received ticket
            return resp;
        DownloadTicketResponse dtresp = (DownloadTicketResponse) resp;
        DownloadTicket dt = dtresp.getTicket();
        DownloadFileRequest dfreq = new DownloadFileRequest(dt);
        Socket fsock = new Socket(dt.getAddress(), dt.getPort());
        fsock.setSoTimeout(2000);
        Response response = SocketUtils.sendAndReceiveObject(fsock, dfreq, Response.class);
        fsock.close();
        if (response instanceof MessageResponse)
            return response;
        DownloadFileResponse dfr = (DownloadFileResponse) response;
        ((StorageService) ObjMgr.getInstance().getService(ctx, "storageService")).writeFile(filename, 0 /* does not matter */, dfr.getContent());
        return dfr;
    }

    @Override
    @Command
    public MessageResponse upload(String filename) throws IOException {
        if (!((StorageService) ObjMgr.getInstance().getService(ctx, "storageService")).exists(filename))
            throw new IOException("file not found!"); // can not return simple message due to test framework
        byte[] fdata = ((StorageService) ObjMgr.getInstance().getService(ctx, "storageService")).readFile(filename);
        UploadRequest ureq = new UploadRequest(filename, 0 /* version does not matter */, fdata);
        return SocketUtils.sendAndReceiveObjectEnc(sock, ureq, MessageResponse.class, aesSession);
    }

    @Override
    @Command
    public MessageResponse logout() throws IOException {
        LogoutRequest lreq = new LogoutRequest();
        this.username = null;
        return SocketUtils.sendAndReceiveObjectEnc(sock, lreq, MessageResponse.class, aesSession);
    }

    @Override
    @Command
    public MessageResponse exit() throws IOException {
        LogoutRequest lreq = new LogoutRequest();
        try {
            MessageResponse mr = SocketUtils.sendAndReceiveObjectEnc(sock, lreq, MessageResponse.class, aesSession);
            cleanup();
            return mr;
        } catch(EOFException e) {
            return new MessageResponse("Successfully logged out.");
        }
    }

    @Override
    @Command
    public MessageResponse readQuorum() throws IOException {
        return new MessageResponse("Read-Quorum is set to " + management.getReadQuorum() + ".");
    }

    @Override
    @Command
    public MessageResponse writeQuorum() throws IOException {
        return new MessageResponse("Read-Quorum is set to " + management.getWriteQuorum() + ".");
    }

    @Override
    @Command
    public MessageResponse topThreeDownloads() throws IOException {
        Map<String, Integer> downloadMap = management.getTopThreeDownloads();

        StringBuilder sb = new StringBuilder();
        if (downloadMap.isEmpty()) {
            sb.append("No downloads yet!\n");
        } else {
            List<Integer> countList = new ArrayList<Integer>();
            countList.addAll(downloadMap.values());
            Collections.sort(countList, Collections.reverseOrder());

            int i = 1;
            Set<String> antiDuplicate = new HashSet<String>();
            for (Integer value : countList) {
                // i know it's O(n^2) but it really sucks without guava
                for (String key : downloadMap.keySet()) {
                    if (downloadMap.get(key).equals(value) && !antiDuplicate.contains(key)) {
                        sb.append(i);
                        sb.append(".\t");
                        sb.append(key);
                        sb.append('\t');
                        sb.append(downloadMap.get(key));
                        sb.append('\n');
                        i++;
                        antiDuplicate.add(key);
                    }
                }
            }
        }

        return new MessageResponse(sb.toString());
    }

    @Override
    @Command
    public MessageResponse subscribe(String filename, Integer count) throws IOException {
        if(management.subscribe(new CallbackInfo(filename, username, count), new CallbackHandler(sh))) {
            return new MessageResponse("Successfully subscribed for file: " + filename);
        } else {
            return new MessageResponse("Subscription failed - you need to log in first");
        }
    }

    @Override
    @Command
    public MessageResponse getProxyPublicKey() throws IOException {
        PublicKey publicKey = management.getProxyPublicKey();

        if(publicKey != null) {
            PKIUtil.writePublicKey(publicKey, clientConfig.getString(CLIENT_KEYS_DIR) + "/" + PROXY_PUBLIC_FILENAME);
            return new MessageResponse("Successfully received public key of Proxy.");
        } else {
            return new MessageResponse("The proxy sent an invalid public key");
        }
    }

    @Override
    @Command
    public MessageResponse setUserPublicKey(String username) throws IOException {
        String filepath = clientConfig.getString(CLIENT_KEYS_DIR) + "/" + username.toLowerCase() + ".pub.pem";
        PublicKey publicKey = PKIUtil.readPublicKey(filepath);

        if (publicKey != null) {
            management.setUserPublicKey(username, publicKey);
        }

        return new MessageResponse("Successfully transmitted public key of user: " + username + ".");
    }
}

package client;

import cli.Shell;
import shared.ICallbackListener;

import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CallbackHandler extends UnicastRemoteObject implements ICallbackListener {

    private static final Logger log = Logger.getLogger(CallbackHandler.class.getName());

    private final Shell shell;

    public CallbackHandler(Shell shell) throws RemoteException {
        super(0);
        this.shell = shell;
    }

    @Override
    public void downloadCountReached(String message) {
        try {
            this.shell.writeLine(message);
        } catch (IOException e) {
            log.log(Level.WARNING, "Error writing notification to shell.");
        }
    }
}

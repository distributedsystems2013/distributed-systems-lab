package model;


import java.io.Serializable;

public class CallbackInfo implements Serializable {

    private String filename;
    private String username;
    private Integer count;

    public String getFilename() {
        return filename;
    }

    public String getUsername() {
        return username;
    }

    public Integer getCount() {
        return count;
    }

    public CallbackInfo(String filename, String username, Integer count) {

        this.filename = filename;
        this.username = username;
        this.count = count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CallbackInfo that = (CallbackInfo) o;

        if (count != null ? !count.equals(that.count) : that.count != null) return false;
        if (filename != null ? !filename.equals(that.filename) : that.filename != null) return false;
        if (username != null ? !username.equals(that.username) : that.username != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = filename != null ? filename.hashCode() : 0;
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (count != null ? count.hashCode() : 0);
        return result;
    }
}

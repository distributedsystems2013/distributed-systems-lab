package util;

public class RMIHelper {

    private static final String RMI_HOSTNAME = "proxy.host";
    private static final String RMI_PORT = "proxy.rmi.port";
    private static final String RMI_BINDING_NAME = "binding.name";

    public static String getRMIUrl(String configfile) {
        Config config = new Config(configfile);

        StringBuilder sb = new StringBuilder();
        sb.append("//");
        sb.append(config.getString(RMI_HOSTNAME));
        sb.append("/");
        sb.append(config.getString(RMI_BINDING_NAME));

        return sb.toString();
    }

    public static Integer getRMIPort(String configfile) {
        Config config = new Config(configfile);

        Integer ret = config.getInt(RMI_PORT);

        if(ret > 0 && ret < 65536) {
            return ret;
        } else {
            throw new IllegalArgumentException("Invalid port number in RMI config");
        }
    }

    public static String getHostname(String configfile) {
        Config config = new Config(configfile);

        return config.getString(RMI_HOSTNAME);
    }

    public static String getServiceName(String configfile) {
        Config config = new Config(configfile);

        return config.getString(RMI_BINDING_NAME);
    }
}

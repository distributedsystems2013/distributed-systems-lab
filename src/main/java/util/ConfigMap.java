package util;

import java.util.Map;

/**
 * Reads the configuration from a {@code .properties} file.
 */
public class ConfigMap extends Config {

	private final Map<String, String> configEntries;

    public ConfigMap(Map<String, String> configEntries) {
        this.configEntries = configEntries;
    }

	public String getString(String key) {
		return configEntries.get(key);
	}

	public int getInt(String key) {
		return Integer.parseInt(getString(key));
	}
}

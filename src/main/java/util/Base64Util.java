package util;

import org.bouncycastle.util.encoders.Base64;

import java.io.*;

public class Base64Util {

    public static String objectToString(Object o) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutput out = new ObjectOutputStream(bos);
        out.writeObject(o);
        return new String(Base64.encode(bos.toByteArray()));
    }

    public static Object stringToObject(String s) throws IOException {
        try {
            ByteArrayInputStream bis = new ByteArrayInputStream(Base64.decode(s.getBytes()));
            ObjectInput ins = new ObjectInputStream(bis);
            return ins.readObject();
        } catch (ClassNotFoundException e) {
            throw new IOException(e);
        }
    }

}

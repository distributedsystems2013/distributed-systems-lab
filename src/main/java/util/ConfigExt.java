package util;

import exception.ConfigException;

import java.util.Enumeration;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * Reads the configuration from a {@code .properties} file.
 */
public final class ConfigExt {

    private Config config;
	private final ResourceBundle bundle;

	/**
	 * Creates an instance of Config which reads configuration data form
	 * {@code .properties} file with given name found in classpath.
	 *
     * @param name the name of the .properties file
     */
	public ConfigExt(final String name) {
        this.config = new Config(name);
		this.bundle = ResourceBundle.getBundle(name);
	}

	/**
	 * Creates an instance of Config which reads configuration data form
	 * {@code .properties} file with given name found in classpath.
	 *
     * @param config Config object
     */
	public ConfigExt(final Config config) {
        this.config = config;
        this.bundle = null;
	}

	/**
	 * Returns the value as String for the given key.
	 *
	 * @param key the property's key
	 * @return String value of the property
	 * @see java.util.ResourceBundle#getString(String)
	 */
	public String getString(String key) throws ConfigException {
        try {
            return config.getString(key);
        } catch (NullPointerException e) {
            throw new ConfigException("key is null");
        } catch (MissingResourceException e) {
            throw new ConfigException("no object for the given key can be found: " + key);
        } catch (ClassCastException e) {
            throw new ConfigException("the object found for the given key is not a string: " + key);
        }
	}

	/**
	 * Returns the value as {@code int} for the given key.
	 *
	 * @param key the property's key
	 * @return int value of the property
	 * @throws NumberFormatException if the String cannot be parsed to an int
	 */
	public int getInt(String key) throws ConfigException {
        try {
            return config.getInt(key);
        } catch (NullPointerException e) {
            throw new ConfigException("key is null");
        } catch (MissingResourceException e) {
            throw new ConfigException("no object for the given key can be found: " + key);
        } catch (ClassCastException e) {
            throw new ConfigException("the object found for the given key is not a string: " + key);
        }
	}

    /**
     * Returns the value as {@code float} for the given key.
     *
     * @param key the property's key
     * @return int value of the property
     * @throws NumberFormatException if the String cannot be parsed to a float
     */
    public float getFloat(String key) throws ConfigException {
        try {
            return Float.parseFloat(getString(key));
        } catch (NumberFormatException e) {
            throw new ConfigException("the object found for the given key is not a float: " + key);
        }
    }

    /**
     * Returns all keys from the config file.
     * @return Enumeration<String>
     */
    public Enumeration<String> getKeys() {
        if(bundle == null) throw new NullPointerException("you have to use 'public ConfigExt(final String name)' constructor to use this method");
        return this.bundle.getKeys();
    }

    /**
     * Returns the original Config object
     * @return Config
     */
    public Config getConfig() {
        return config;
    }

}

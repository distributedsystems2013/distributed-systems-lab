package util;

import message.Request;
import message.Response;
import shared.hmac.HMACMessage;
import shared.hmac.HMACSession;
import shared.hmac.TamperedMessageResponse;
import shared.secured.SecuredMessage;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.logging.Logger;

public class SocketUtils {
    private static final Logger log = Logger.getLogger( SocketUtils.class.getName() );

    public static <O extends Response, I extends Request> O sendAndReceiveObject(Socket sock, I sendObject, Class<O> clazz) throws IOException {
        ObjectOutputStream ous = new ObjectOutputStream(sock.getOutputStream());
        ous.writeObject(sendObject);
        ObjectInputStream ins = new ObjectInputStream(sock.getInputStream());
        try {
            return (O) ins.readObject();
        } catch (ClassNotFoundException e) {
            log.severe("got invalid response!");
        }
        return null;
    }
    public static <O extends Response, I extends Request> O sendAndReceiveObjectHMAC(Socket sock, I sendObject, Class<O> clazz, String keyPath) throws IOException {
        return sendAndReceiveObjectHMAC(sock, sendObject, clazz, keyPath, 5);
    }


    public static <O extends Response, I extends Request> O sendAndReceiveObjectHMAC(Socket sock, I sendObject, Class<O> clazz, String keyPath, int retry) throws IOException {
        if(retry == 0)
            throw new IOException("got too much corrupted messages, stop retrying");
        HMACSession hmacSession = new HMACSession(keyPath);
        ObjectOutputStream ous = new ObjectOutputStream(sock.getOutputStream());
        ous.writeObject(hmacSession.buildMessage(sendObject));
        ObjectInputStream ins = new ObjectInputStream(sock.getInputStream());
        try {
            Object inc = ins.readObject();
            try {
                if(inc instanceof TamperedMessageResponse)
                    throw new IOException("tampered msg");
                return (O) hmacSession.verifyMessage((HMACMessage) inc);
            } catch (IOException e) {
                return sendAndReceiveObjectHMAC(sock, sendObject, clazz, keyPath, --retry); // send again, if tampered
            }
        } catch (ClassNotFoundException e) {
            log.severe("got invalid response!");
        }
        return null;
    }

    public static <O extends Response, I extends Request> O sendAndReceiveObjectEnc(Socket sock, I sendObject, Class<O> clazz, AesSession aesSession) throws IOException {
        if(aesSession == null)
            throw new IOException("no session, are you logged in?");
        ObjectOutputStream ous = new ObjectOutputStream(sock.getOutputStream());
        ous.writeObject(new SecuredMessage(aesSession.objectToString(sendObject)));
        ObjectInputStream ins = new ObjectInputStream(sock.getInputStream());
        try {
            Object inc = ins.readObject();
            if(!(inc instanceof SecuredMessage))
                throw new IOException("received unexpected message");
            return (O) aesSession.stringToObject(((SecuredMessage)inc).getObject());
        } catch (ClassNotFoundException e) {
            log.severe("got invalid response!");
        }
        return null;
    }

}

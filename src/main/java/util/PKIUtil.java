package util;

import org.bouncycastle.openssl.PEMReader;
import org.bouncycastle.openssl.PEMWriter;
import org.bouncycastle.openssl.PasswordFinder;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PKIUtil {

    private static final Logger log = Logger.getLogger(PKIUtil.class.getName());

    public static PublicKey readPublicKey(final String filename) {
        try {
            PEMReader in = new PEMReader(new FileReader(filename));

            PublicKey publicKey = (PublicKey) in.readObject();
            in.close();

            return publicKey;
        } catch (FileNotFoundException e) {
            log.log(Level.WARNING, "Public key file not found: \"" + filename + "\"");
        } catch (IOException e) {
            log.log(Level.WARNING, "Error reading public key from file: \"" + filename + "\"");
        }
        return null;
    }

    public static PrivateKey readPrivateKey(final String filename, final String password) {
        try {
            PEMReader in = new PEMReader(new FileReader(filename), new PasswordFinder() {
                @Override
                public char[] getPassword() {
                    return password.toCharArray();
                }
            });

            KeyPair keyPair = (KeyPair) in.readObject();
            in.close();

            return keyPair.getPrivate();

        } catch (FileNotFoundException e) {
            log.log(Level.WARNING, "Private key file not found: \"" + filename + "\"");
        } catch (IOException e) {
            log.log(Level.WARNING, "Error reading private key from file: \"" + filename + "\"");
        }
        return null;
    }

    public static void writePublicKey(final PublicKey publicKey, final String filename) {
        try {
            PEMWriter out = new PEMWriter(new FileWriter(filename));

            out.writeObject(publicKey);
            out.close();

        } catch (IOException e) {
            log.log(Level.WARNING, "Error writing public key to file: \"" + filename + "\"");
        }
    }
}

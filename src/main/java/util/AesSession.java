package util;

import org.bouncycastle.util.encoders.Base64;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import java.io.*;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class AesSession {

    private static final String ALGORITHM = "AES/CTR/NoPadding";

    private Cipher encCipher, decCipher;

    public AesSession(SecretKey secretKey, byte[] iv) throws IOException {
        try {
            encCipher = Cipher.getInstance(ALGORITHM);
            encCipher.init(Cipher.ENCRYPT_MODE, secretKey, new IvParameterSpec(iv));
            decCipher = Cipher.getInstance(ALGORITHM);
            decCipher.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(iv));
        } catch (NoSuchAlgorithmException e) {
            throw new IOException(e);
        } catch (NoSuchPaddingException e) {
            throw new IOException(e);
        } catch (InvalidKeyException e) {
            throw new IOException(e);
        } catch (InvalidAlgorithmParameterException e) {
            throw new IOException(e);
        }

    }

    public String objectToString(Object o) throws IOException {
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutput out = new ObjectOutputStream(bos);
            out.writeObject(o);
            return new String(Base64.encode(encCipher.doFinal(bos.toByteArray())));
        } catch (IllegalBlockSizeException e) {
            throw new IOException(e);
        } catch (BadPaddingException e) {
            throw new IOException(e);
        }
    }

    public Object stringToObject(String s) throws IOException {
        try {
            ByteArrayInputStream bis = new ByteArrayInputStream(decCipher.doFinal(Base64.decode(s.getBytes())));
            ObjectInput ins = new ObjectInputStream(bis);
            return ins.readObject();
        } catch (ClassNotFoundException e) {
            throw new IOException(e);
        } catch (BadPaddingException e) {
            throw new IOException(e);
        } catch (IllegalBlockSizeException e) {
            throw new IOException(e);
        }
    }
}

package server;

import shared.ReflectionSession;
import shared.hmac.HMACReflectionSession;

import java.net.Socket;

public class FileServerSession extends HMACReflectionSession {

    public FileServerSession(String ctx, Socket s) {
        super(ctx, s);
        this.registerHandlers(new FileServerHandler(this));
    }
}

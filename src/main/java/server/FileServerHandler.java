package server;

import message.Response;
import message.request.*;
import message.response.*;
import model.DownloadTicket;
import proxy.ProxySession;
import shared.Handler;
import shared.ObjMgr;
import shared.StorageService;
import util.ChecksumUtils;

import java.io.IOException;

public class FileServerHandler implements IFileServer {

    private FileServerSession session;

    public FileServerHandler(FileServerSession session) {
        this.session = session;
    }

    @Override
    @Handler(handle = ListRequest.class, integrity = true)
    public Response list() throws IOException {
        return new ListResponse(((StorageService)ObjMgr.getInstance().getService(session.getCtx(), "storageService")).listFiles());
    }

    @Override
    @Handler(handle = DownloadFileRequest.class)
    public Response download(DownloadFileRequest request) throws IOException {
        if(request.getTicket() == null)
            return new MessageResponse("invalid ticket");
        DownloadTicket dt = request.getTicket();
        StorageService storageService = ((StorageService)ObjMgr.getInstance().getService(session.getCtx(), "storageService"));
        if(!ChecksumUtils.verifyChecksum(dt.getUsername(), storageService.getFileByName(dt.getFilename()), storageService.version(dt.getFilename()), dt.getChecksum()))
            return new MessageResponse("got invalid downloadticket!");
        byte[] buf;
        try {
            buf = storageService.readFile(dt.getFilename());
        } catch (IOException e) {
            return new MessageResponse("couldn't upload file due to: " + e.getMessage());
        }

        return new DownloadFileResponse(dt, buf);
    }

    @Override
    @Handler(handle = InfoRequest.class, integrity = true)
    public Response info(InfoRequest request) throws IOException {
        if(!((StorageService)ObjMgr.getInstance().getService(session.getCtx(), "storageService")).exists(request.getFilename()))
            return new MessageResponse("file does not exist");
        return new InfoResponse(request.getFilename(), ((StorageService)ObjMgr.getInstance().getService(session.getCtx(), "storageService")).size(request.getFilename()));
    }

    @Override
    @Handler(handle = VersionRequest.class, integrity = true)
    public Response version(VersionRequest request) throws IOException {
        if(!((StorageService)ObjMgr.getInstance().getService(session.getCtx(), "storageService")).exists(request.getFilename()))
            return new MessageResponse("file does not exist");
        return new VersionResponse(request.getFilename(), ((StorageService)ObjMgr.getInstance().getService(session.getCtx(), "storageService")).version(request.getFilename()));
    }

    @Override
    @Handler(handle = UploadRequest.class, integrity = true)
    public MessageResponse upload(UploadRequest request) throws IOException {
        try {
            ((StorageService) ObjMgr.getInstance().getService(session.getCtx(), "storageService")).writeFile(request.getFilename(), request.getVersion(), request.getContent());
        } catch (IOException e) {
            return new MessageResponse("couldn't upload file due to: " + e.getMessage());
        }
        return new MessageResponse("upload succeeded");
    }
}

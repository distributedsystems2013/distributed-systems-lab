package server;

import cli.Command;
import cli.Shell;
import message.response.MessageResponse;
import shared.GenericSocketAcceptor;
import shared.ObjMgr;
import shared.StorageService;
import util.Config;

import java.io.IOException;

public class FileServerCli implements IFileServerCli {

    private GenericSocketAcceptor<FileServerSession> sa;
    private HeartbeatService heartbeatService;

    public static void main(String[] args) {
        Shell sh = new Shell("fscli", System.out, System.in);
        new FileServerCli(new Config(args[0]), sh);
        sh.run();
    }

    public FileServerCli(Config conf, Shell sh) {
        int fileserverListenerPort = conf.getInt("tcp.port");
        String proxyHost = conf.getString("proxy.host");
        int proxyUdpPort = conf.getInt("proxy.udp.port");
        int aliveTick = conf.getInt("fileserver.alive");
        String storagePath = conf.getString("fileserver.dir");
        String ctx = "fs"+fileserverListenerPort;

        ObjMgr.getInstance().addService(ctx, "storageService", new StorageService());
        ObjMgr.getInstance().addService(ctx, "config", conf);

        heartbeatService = new HeartbeatService(proxyHost, proxyUdpPort, fileserverListenerPort);
        heartbeatService.start(aliveTick);

        sa = new GenericSocketAcceptor<FileServerSession>(ctx, fileserverListenerPort, FileServerSession.class);
        sa.start();

        try {
            ((StorageService)ObjMgr.getInstance().getService(ctx, "storageService")).setStoragePath(storagePath);
        } catch (IOException e) {
            throw new RuntimeException("storage path is not a directory !");
        }

        sh.register(this);
    }

    @Override
    @Command
    public MessageResponse exit() throws IOException {
        heartbeatService.stop();
        sa.interrupt();
        return new MessageResponse("shutdown complete");
    }

}

package server;


import convert.ConversionService;

import java.io.IOException;
import java.net.*;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Logger;

public class HeartbeatService {
    private static final Logger log = Logger.getLogger( HeartbeatService.class.getName() );
    private DatagramSocket socket = null;
    private ConversionService conversionService = new ConversionService();
    private Timer timer;
    private DatagramPacket packet;

    public HeartbeatService(String hostname, int destPort, int fsPort) {
        try {
            timer = new Timer();
            socket = new DatagramSocket();
            String aliveMsg = "!alive " + fsPort;
            byte[] buf = conversionService.convert(aliveMsg, byte[].class);
            packet = new DatagramPacket(buf, buf.length, InetAddress.getByName(hostname), destPort);
        } catch (SocketException e) {
            log.severe("can not open socket");
            stop();
        } catch (UnknownHostException e) {
            log.severe("host not found");
            stop();
        }
    }

    public void start(int schedule) {
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    socket.send(packet);
                } catch (IOException e) {
                    log.severe("can not send alive");
                    stop();
                }
            }
        }, 0, schedule);
    }

    public void stop() {
        timer.cancel();
        if(socket != null && !socket.isClosed())
            socket.close();
    }
}

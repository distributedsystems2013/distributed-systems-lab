package proxy;

import shared.GenericSession;

import java.util.HashSet;
import java.util.Set;

public class User {
    private String name, password;
    private Long credits;
    private boolean online;
    private HashSet<GenericSession> sessions = new HashSet<GenericSession>();

    public User(String name, String password) {
        this.name = name;
        this.password = password;
        this.credits = new Long(0);
    }

    public Set<GenericSession> getSessions() {
        return sessions;
    }

    public void addSession(GenericSession session) {
        synchronized (sessions) {
            this.sessions.add(session);
        }
    }

    public void removeSession(GenericSession session) {
        synchronized (sessions) {
            this.sessions.remove(session);
        }
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public long getCredits() {
        return credits;
    }

    public void addCredits(long credits) {
        synchronized (this.credits) {
            this.credits += credits;
        }
    }

    public void removeCredits(long credits) {
        synchronized (this.credits) {
            this.credits -= credits;
        }
    }

    public synchronized boolean isOnline() {
        return !sessions.isEmpty();
    }

    @Override
    public String toString() {

        return "User{" +
                "name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", credits=" + credits +
                ", online=" + online +
                '}';
    }
}

package proxy;

import message.Response;
import message.request.*;
import message.response.*;
import model.DownloadTicket;
import shared.Handler;
import shared.ObjMgr;
import util.ChecksumUtils;
import util.Config;
import util.SocketUtils;

import java.io.IOException;
import java.net.Socket;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class ProxyHandler implements IProxy {

    private ProxySession session;
    private String hMacKeyPath;

    public ProxyHandler(ProxySession session) {
        this.session = session;
        Config config = (Config) ObjMgr.getInstance().getService(session.getCtx(), "config");
        hMacKeyPath = config.getString("hmac.key");
    }

    @Override
    @Deprecated
    //@Handler(handle = LoginRequest.class)
    public LoginResponse login(LoginRequest request) throws IOException {
        if(session.isLoggedIn())
            logout();
        User login = UserService.getInstance().login(request.getUsername(), request.getPassword(), session);
        if(login == null)
            return new LoginResponse(LoginResponse.Type.WRONG_CREDENTIALS);
        session.setUser(login);
        return new LoginResponse(LoginResponse.Type.SUCCESS);
    }

    @Override
    @Handler(handle = CreditsRequest.class, secured = true)
    public Response credits() throws IOException {
        return new CreditsResponse(session.getUser().getCredits());
    }

    @Override
    @Handler(handle = BuyRequest.class, secured = true)
    public Response buy(BuyRequest credits) throws IOException {
        session.getUser().addCredits(credits.getCredits());
        return new BuyResponse(session.getUser().getCredits());
    }

    @Override
    @Handler(handle = ListRequest.class, secured = true)
    public Response list() throws IOException {
        Set<String> filenames = new TreeSet<String>();
        List<FileServer> fileServerList = FileserverService.getInstance().getQuorum().getReadQuorum();
        if(fileServerList.size() == 0)
            return new MessageResponse("no fileservers available");
        for(FileServer fileServer : fileServerList) {
            Socket socket = fileServer.openSocket();
            Response response = SocketUtils.sendAndReceiveObjectHMAC(socket, new ListRequest(), Response.class, hMacKeyPath);
            if(response instanceof ListResponse)
                filenames.addAll(((ListResponse) response).getFileNames());
            socket.close();
        }
        return new ListResponse(filenames);
    }

    @Override
    @Handler(handle = DownloadTicketRequest.class, secured = true)
    public Response download(DownloadTicketRequest request) throws IOException {
        String filename = request.getFilename();
        FileInfo fileInfo = FileserverService.getInstance().getFileInfoWithHighestVersionOfFile(request.getFilename(), hMacKeyPath);
        if(fileInfo == null)
            return new MessageResponse("no fileservers online!");
        if(!fileInfo.fileDoesExist())
            return new MessageResponse("file does not exist!");
        FileServer fileServer = fileInfo.getFileserver();
        Response response = SocketUtils.sendAndReceiveObjectHMAC(fileServer.openSocket(), new InfoRequest(filename), Response.class, hMacKeyPath);
        if(response instanceof MessageResponse)
            return response; // file does not exisist / fileserver problem
        InfoResponse ir = (InfoResponse) response;
        String username = session.getUser().getName();
        String checksum = ChecksumUtils.generateChecksum(username, filename, fileInfo.getVersion(), ir.getSize());
        User u = session.getUser();
        synchronized (u) {
            if(u.getCredits() < ir.getSize())
                return new MessageResponse("not enough credits!");
            u.removeCredits(ir.getSize());
        }
        FileserverService.getInstance().addUsage(fileInfo.getFileserver(), ir.getSize());
        FileserverService.getInstance().incrementFileDownloadCount(filename);
        DownloadTicket dt = new DownloadTicket(username, filename, checksum, fileServer.getAddress(), fileServer.getPort());
        return new DownloadTicketResponse(dt);
    }

    @Override
    @Handler(handle = UploadRequest.class, secured = true)
    public MessageResponse upload(UploadRequest request) throws IOException {
        FileserverService.getInstance().getQuorum().setQuorumSizeFix();
        FileInfo fileInfo = FileserverService.getInstance().getFileInfoWithHighestVersionOfFile(request.getFilename(), hMacKeyPath);
        if(fileInfo == null)
            return new MessageResponse("no online fileservers, can not store file!");
        int highestVersion = fileInfo.fileDoesExist() ? fileInfo.getVersion() + 1 : 1;
        UploadRequest newUploadRequest = new UploadRequest(request.getFilename(), highestVersion, request.getContent());
        boolean uploaded = false;
        for(FileServer fs : FileserverService.getInstance().getQuorum().getWriteQuorum()) {
            try {
                Socket s = fs.openSocket();
                MessageResponse response = SocketUtils.sendAndReceiveObjectHMAC(s, newUploadRequest, MessageResponse.class, hMacKeyPath);
                if(response.getMessage().contains("succeeded")) {
                    uploaded = true;
                    FileserverService.getInstance().addUsage(fs, request.getContent().length);
                }
                s.close();
            } catch (IOException e) { /* ignore, upload variable does everything */}
        }
        if(!uploaded)
            return new MessageResponse("couldn't upload to any fileserver!");
        session.getUser().addCredits(request.getContent().length * 2);
        return new MessageResponse("uploaded file successfully");
    }

    @Override
    @Handler(handle = LogoutRequest.class, secured = true)
    public MessageResponse logout() throws IOException {
        UserService.getInstance().logout(session.getUser().getName(), session);
        session.setUser(null);
        return new MessageResponse("Successfully logged out.");
    }
}

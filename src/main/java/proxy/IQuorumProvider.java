package proxy;

import java.util.List;

public interface IQuorumProvider {
    List<FileServer> getQuorumFileservers();
}

package proxy;

import model.CallbackInfo;
import shared.ICallbackListener;
import shared.IManagement;
import util.Config;
import util.PKIUtil;
import util.RMIHelper;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.security.PublicKey;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ManagementService extends UnicastRemoteObject implements IManagement {

    private static final Logger log = Logger.getLogger(ManagementService.class.getName());
    public static ManagementService managementService = null;
    private static Registry registry;

    private static final String PROXY_CONFIG_NAME = "proxy";
    private static final String PROXY_KEY_DIR = "keys.dir";
    private static final String PROXY_PUBKEY_FILE = "proxy.pub.pem";
    private static final String RMI_CONFIG_NAME = "mc";

    private Map<CallbackInfo, ICallbackListener> callbackMap = new HashMap<CallbackInfo, ICallbackListener>();

    private FileserverService fileserverService;


    private ManagementService() throws RemoteException {
        super(0);
        this.fileserverService = FileserverService.getInstance();
    }

    public static ManagementService getInstance() {
        try {
            if (managementService == null) {
                managementService = new ManagementService();
                initRMI();
            }
        } catch (RemoteException e) {
            log.log(Level.WARNING, "Error creating RMI Service");
        }
        return managementService;
    }

    private static void initRMI() throws RemoteException {
        registry = LocateRegistry.createRegistry(RMIHelper.getRMIPort(RMI_CONFIG_NAME));
        registry.rebind(RMIHelper.getServiceName(RMI_CONFIG_NAME), ManagementService.managementService);
    }

    public void shutdown() throws RemoteException {
        //Registry registry = LocateRegistry.createRegistry(RMIHelper.getRMIPort(RMI_CONFIG_NAME));
        UnicastRemoteObject.unexportObject(registry, true);
    }

    public synchronized void downloadCountNotify(String filename, Integer count) {
        List<CallbackInfo> toRemove = new ArrayList<CallbackInfo>();

        for (CallbackInfo callbackInfo : callbackMap.keySet()) {
            try {
                if (callbackInfo.getFilename().equals(filename) && callbackInfo.getCount().equals(count)) {
                    callbackMap.get(callbackInfo).downloadCountReached("Notification: " + filename + " got downloaded " + count + " times!.");
                    toRemove.add(callbackInfo);
                }
            } catch (RemoteException e) {
                log.log(Level.WARNING, "Error calling the callback"); // 8-)
            }
        }

        for (CallbackInfo callbackInfo : toRemove) {
            this.callbackMap.remove(callbackInfo);
        }
    }

    public void removeCallbacks(String username) {
        List<CallbackInfo> toRemove = new ArrayList<CallbackInfo>();
        for (CallbackInfo callbackInfo : this.callbackMap.keySet()) {
            if (callbackInfo.getUsername().equals(username)) {
                toRemove.add(callbackInfo);
            }
        }

        for (CallbackInfo callbackInfo : toRemove) {
            this.callbackMap.remove(callbackInfo);
        }
    }

    @Override
    public Integer getReadQuorum() throws RemoteException {
        return this.fileserverService.getQuorum().getReadQuorumSize();
    }

    @Override
    public Integer getWriteQuorum() throws RemoteException {
        return this.fileserverService.getQuorum().getWriteQuorumSize();
    }

    @Override
    public Map<String, Integer> getTopThreeDownloads() throws RemoteException {
        // i want fucking guava
        Map<String, Integer> unsortedMap = this.fileserverService.getFileDownloadCountMap();

        if (unsortedMap.size() <= 3) {
            return unsortedMap;
        } else {
            List<Integer> countList = new ArrayList<Integer>();
            countList.addAll(unsortedMap.values());
            Collections.sort(countList, Collections.reverseOrder());

            Map<String, Integer> retMap = new HashMap<String, Integer>();
            while (retMap.size() < 3) {
                // i know it's O(n^2) but it really sucks without guava
                for (String key : unsortedMap.keySet()) {
                    if (unsortedMap.get(key) > countList.get(0)) {
                        retMap.put(key, unsortedMap.get(key));
                    }
                }
                countList.remove(0);
            }

            return retMap;
        }
    }

    @Override
    public PublicKey getProxyPublicKey() throws RemoteException {
        Config proxy = new Config(PROXY_CONFIG_NAME);
        return PKIUtil.readPublicKey(proxy.getString(PROXY_KEY_DIR) + "/" + PROXY_PUBKEY_FILE);
    }

    @Override
    public void setUserPublicKey(String username, PublicKey userPublicKey) throws RemoteException {
        if (username != null && userPublicKey != null) {
            Config proxyConfig = new Config(PROXY_CONFIG_NAME);
            PKIUtil.writePublicKey(userPublicKey, proxyConfig.getString(PROXY_KEY_DIR) + "/" + username.toLowerCase() + ".pub.pem");
        }
    }

    // there is only user-, not session-correlation! if there is a way to correlate session SAFELY please let me know
    @Override
    public synchronized boolean subscribe(CallbackInfo callbackInfo, ICallbackListener iCallback) throws RemoteException {
        if (UserService.getInstance().isLoggedIn(callbackInfo.getUsername())) {
            synchronized (fileserverService.getFileDownloadCountMap()) {
                if (fileserverService.getFileDownloadCountMap().containsKey(callbackInfo.getFilename()) &&
                        fileserverService.getFileDownloadCountMap().get(callbackInfo.getFilename()) >= callbackInfo.getCount()) {
                    iCallback.downloadCountReached("Notification: " + callbackInfo.getFilename() + " already got downloaded " + callbackInfo.getCount() + " times!.");
                    return false;
                } else {
                    this.callbackMap.put(callbackInfo, iCallback);
                    return true;
                }
            }
        } else {
            // user is not logged in => gtfo
            return false;
        }
    }
}

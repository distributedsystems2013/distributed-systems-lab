package proxy;


import model.UserInfo;
import shared.GenericSession;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class UserService {

    private static UserService instance = null;
    private ConcurrentHashMap<String, User> users = new ConcurrentHashMap<String, User>();

    private UserService() {
        loadUsers();
    }

    private void loadUsers() {
        ResourceBundle bundle = ResourceBundle.getBundle("user");
        Set<String> users = new HashSet<String>();
        Enumeration<String> keys = bundle.getKeys();
        while(keys.hasMoreElements()) {
            String key = keys.nextElement();
            if(key.endsWith("password")) {
                users.add(key.split("\\.")[0]); // add username
            }
        }
        for(String username : users) {
            User user = new User(username, bundle.getString(username + ".password"));
            user.addCredits(Long.parseLong(bundle.getString(username + ".credits")));
            this.users.put(username, user);
        }
    }

    public static UserService getInstance() {
        if(instance == null) {
            instance = new UserService();
        }
        return instance;
    }

    public User login(String username, GenericSession session) {
        if(users.containsKey(username)) {
            User user = users.get(username);
            user.addSession(session);
            return user;
        }
        return null;
    }

    @Deprecated
    public User login(String username, String password, GenericSession session) {
        if(users.containsKey(username) && users.get(username).getPassword().equals(password)) {
            User user = users.get(username);
            user.addSession(session);
            return user;
        }
        return null;
    }

    public void logout(String username, GenericSession session) {
        if(users.containsKey(username)) {
            users.get(username).removeSession(session);
        }
        ManagementService.getInstance().removeCallbacks(username);
    }

    public synchronized List<UserInfo> listUsers() {
        List<UserInfo> userInfoList = new ArrayList<UserInfo>();
        for(User user: users.values()) {
            userInfoList.add(new UserInfo(user.getName(), user.getCredits(), user.isOnline()));
        }
        return userInfoList;
    }

    /**
     * best we can do (session on user-level and not at connection-level); the problem
     * is correlating rmi and tcp connections properly since only 6 rmi methods are allowed
     */
    public boolean isLoggedIn(String username) {
        if (username != null && users.containsKey(username)) {
            User user = users.get(username);
            return user.isOnline();
        } else {
            return false;
        }
    }
}

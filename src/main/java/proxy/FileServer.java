package proxy;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Comparator;

public class FileServer {
    private InetAddress address;
    private int port;

    private long lastHeartbeat;
    private long usage;
    private boolean online;

    public FileServer(InetAddress address, int port) {
        this.address = address;
        this.port = port;
    }

    public Socket openSocket() throws IOException {
        Socket socket = new Socket(address, port);
        if(socket == null)
            throw new IOException("fileserver not available");
        socket.setSoTimeout(2000);
        return socket;
    }

    public InetAddress getAddress() {
        return address;
    }

    public int getPort() {
        return port;
    }

    public long getLastHeartbeat() {
        return lastHeartbeat;
    }

    public void setLastHeartbeat(long lastHeartbeat) {
        this.lastHeartbeat = lastHeartbeat;
    }

    public void setUsage(long usage) {
        this.usage = usage;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }

    public long getUsage() {
        return usage;
    }

    public boolean isOnline() {
        return online;
    }

    @Override
    public String toString() {
        return "FileServer{" +
                "address=" + address +
                ", port=" + port +
                ", lastHeartbeat=" + lastHeartbeat +
                ", usage=" + usage +
                ", online=" + online +
                '}';
    }

    public static class UsageComparator implements Comparator<FileServer> {
        @Override
        public int compare(FileServer o1, FileServer o2) {
            return new Long(o1.getUsage()).compareTo(o2.getUsage());
        }
    }
}

package proxy;

import message.Response;
import message.request.VersionRequest;
import message.response.VersionResponse;
import model.FileServerInfo;
import util.SocketUtils;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

public class FileserverService implements IQuorumProvider {

    private static final Logger log = Logger.getLogger(FileserverService.class.getName());

    private static FileserverService instance = null;
    private Listener listener;
    private Timer checkAliveTimer;

    private ConcurrentHashMap<IPPortContainer, FileServer> fileserverMap = new ConcurrentHashMap<IPPortContainer, FileServer>();
    private ConcurrentHashMap<String, AtomicInteger> fileDownloadCountMap = new ConcurrentHashMap<String, AtomicInteger>();
    private Quorum quorum;
    private int port, timeout, refresh;

    private FileserverService() {
        quorum = new Quorum(this);
    }

    public static FileserverService getInstance() {
        if (instance == null)
            instance = new FileserverService();
        return instance;
    }

    public void startListener(int port, int timeout, int refresh) {
        this.port = port;
        this.timeout = timeout;

        listener = new Listener();
        listener.start();

        checkAliveTimer = new Timer();
        checkAliveTimer.schedule(new CheckAlive(), 0, refresh);
    }

    public void stopListener() {
        checkAliveTimer.cancel();
        listener.interrupt();
    }

    public void addUsage(FileServerInfo fileServerInfo, long usage) {
        addUsage(fileServerInfo.getAddress(), fileServerInfo.getPort(), usage);
    }

    private void addUsage(InetAddress server, int port, long usage) {
        FileServer fileServer = fileserverMap.get(new IPPortContainer(server, port));
        addUsage(fileServer, usage);
    }

    public void addUsage(FileServer fileServer, long usage) {
        synchronized (fileServer) {
            fileServer.setUsage(fileServer.getUsage() + usage);
        }
    }

    public FileServerInfo getLeastLoadedOnlineServer() {
        FileServerInfo lls = null;
        for (FileServerInfo fsi : getFileServers()) {
            if (!fsi.isOnline())
                continue;
            if (lls == null || lls.getUsage() > fsi.getUsage())
                lls = fsi;
        }
        return lls;
    }

    public Socket getFileServerSocket() throws IOException {
        FileServerInfo fsi = getLeastLoadedOnlineServer();
        if (fsi == null)
            throw new IOException("fileserver not available");
        return fileserverMap.get(new IPPortContainer(fsi.getAddress(), fsi.getPort())).openSocket();
    }

    public List<FileServerInfo> getFileServers() {
        List<FileServerInfo> lfsi = new ArrayList<FileServerInfo>();
        for (FileServer fs : fileserverMap.values()) {
            FileServerInfo fsi = new FileServerInfo(fs.getAddress(), fs.getPort(), fs.getUsage(), fs.isOnline());
            lfsi.add(fsi);
        }
        return lfsi;
    }

    public List<FileServer> getOnlineFileServers() {
        List<FileServer> fsl = new ArrayList<FileServer>();
        for (FileServer fs : fileserverMap.values()) {
            if (fs.isOnline())
                fsl.add(fs);
        }
        return fsl;
    }

    public Quorum getQuorum() {
        return quorum;
    }

    public List<FileServer> getQuorumFileservers() {
        return getOnlineFileServers();
    }

    public FileInfo getFileInfoWithHighestVersionOfFile(String filename, String hMacKeyPath) throws IOException {
        List<FileServer> fileServerList = FileserverService.getInstance().getQuorum().getReadQuorum();
        if (fileServerList.size() == 0)
            return null;

        // search for fileservers which have the highest version number
        int highestVersion = -1;
        List<FileServer> highestFileServerList = new ArrayList<FileServer>();
        for (FileServer fileServer : fileServerList) {
            Socket socket = fileServer.openSocket();
            Response response = SocketUtils.sendAndReceiveObjectHMAC(socket, new VersionRequest(filename), Response.class, hMacKeyPath);
            if (response instanceof VersionResponse) {
                int version = ((VersionResponse) response).getVersion();
                if (version > highestVersion) {
                    highestVersion = version;
                    highestFileServerList = new ArrayList<FileServer>();
                }
                if (version == highestVersion) {
                    highestFileServerList.add(fileServer);
                }
            } else {
                /* errors getting ignored
                 * this could be, if the server is dead, or the file is not on the server
                 */
            }
            socket.close();
        }

        // choose from the preselected fileservers, the one, with the lowest usage
        FileServer choosenFileServer = null;
        for (FileServer fileServer : highestFileServerList) {
            if (choosenFileServer == null) {
                choosenFileServer = fileServer;
            } else {
                if (choosenFileServer.getUsage() > fileServer.getUsage()) {
                    choosenFileServer = fileServer;
                }
            }
        }

        return new FileInfo(filename, choosenFileServer, highestVersion);
    }

    private class Listener extends Thread {
        private DatagramSocket socket = null;

        private Listener() {
            setDaemon(true);
        }

        @Override
        public void run() {
            try {
                socket = new DatagramSocket(port);
                while (!isInterrupted()) {
                    byte[] receivedData = new byte[1024]; // should be enough to store !isAlive <port>
                    DatagramPacket packet = new DatagramPacket(receivedData, receivedData.length);

                    socket.receive(packet);
                    String receivedString = new String(packet.getData()).trim();
                    log.finer("listener received from " + packet.getAddress() + " data: " + receivedString);

                    if (!receivedString.startsWith("!alive ")) {
                        log.warning("invalid message received");
                        continue;
                    }
                    int port = Integer.parseInt(receivedString.substring(7));

                    if (fileserverMap.containsKey(new IPPortContainer(packet.getAddress(), port))) {
                        FileServer fs = fileserverMap.get(new IPPortContainer(packet.getAddress(), port));
                        synchronized (fs) {
                            fs.setOnline(true);
                            fs.setLastHeartbeat(System.currentTimeMillis());
                        }
                    } else {
                        try {
                            FileServer fs = new FileServer(packet.getAddress(), port);
                            fs.setOnline(true);
                            fs.setUsage(0);
                            fs.setLastHeartbeat(System.currentTimeMillis());
                            fileserverMap.put(new IPPortContainer(packet.getAddress(), port), fs);
                        } catch (NumberFormatException nfe) {
                            log.info("invalid message received");
                            continue;
                        }
                    }
                }
            } catch (IOException e) {
                log.severe("listener died!, no fileservers will be available !");
                interrupt();
            }
        }

        @Override
        public void interrupt() {
            super.interrupt();
            if (socket != null && !socket.isClosed())
                socket.close();
        }
    }

    private class CheckAlive extends TimerTask {
        @Override
        public void run() {
            for (FileServer fs : fileserverMap.values()) {
                synchronized (fs) {
                    if (fs.getLastHeartbeat() + timeout < System.currentTimeMillis())
                        fs.setOnline(false);
                }
            }
        }
    }

    private class IPPortContainer {
        private InetAddress ip;
        private int port;

        private IPPortContainer(InetAddress ip, int port) {
            this.ip = ip;
            this.port = port;
        }

        private InetAddress getIp() {
            return ip;
        }

        private void setIp(InetAddress ip) {
            this.ip = ip;
        }

        private int getPort() {
            return port;
        }

        private void setPort(int port) {
            this.port = port;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            IPPortContainer that = (IPPortContainer) o;

            if (port != that.port) return false;
            if (!ip.equals(that.ip)) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = ip.hashCode();
            result = 31 * result + port;
            return result;
        }
    }

    public void incrementFileDownloadCount(String filename) {
        synchronized (this.fileDownloadCountMap) {
            if (this.fileDownloadCountMap.containsKey(filename)) {
                this.fileDownloadCountMap.get(filename).incrementAndGet();
            } else {
                this.fileDownloadCountMap.put(filename, new AtomicInteger(1));
            }
            ManagementService.getInstance().downloadCountNotify(filename, this.fileDownloadCountMap.get(filename).intValue());
        }
    }

    public Map<String, Integer> getFileDownloadCountMap() {
        Map<String, Integer> retMap = new HashMap<String, Integer>();
        for (String key : fileDownloadCountMap.keySet()) {
            retMap.put(key, fileDownloadCountMap.get(key).intValue());
        }
        return retMap;
    }
}

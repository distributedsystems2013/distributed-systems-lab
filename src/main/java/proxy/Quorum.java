package proxy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This Quorum class is an implementation of the Gifford's scheme, where out of a collection of servers, some servers
 * are in a read-quorum and some servers are in a write-quorum.
 *
 * The important thing here is, that the following conditions are hold:
 *
 * readQuorumSize + writeQuorumSize > size
 * writeQuorumSize > size / 2
 * (size is the number of all servers)
 *
 * The readQuorumSize and writeQuorumSize are calculated dynamically at first, based on the size of servers from the
 * QuorumProvider. If setQuorumSizeFix() is called, both sizes will be calculated one last time and  from this point
 * of time, the values are fixed (necessary for our assignment, when the first !upload command is called).
 *
 * The servers are chosen the following way: we take all servers, sort them by their usage ascending and choose the
 * first x servers, where x is the readQuorumSize or the writeQuorumSize, depending on the getter method which got
 * called (getReadQuorum() or getWriteQuorum()).
 */
public class Quorum {

    private IQuorumProvider provider;
    private boolean isQuorumSizeFix = false;
    private int readQuorumSize;
    private int writeQuorumSize;

    public Quorum(IQuorumProvider provider) {
        this.provider = provider;
    }

    public synchronized List<FileServer> getReadQuorum() {
        List<FileServer> orderedFileServers = new ArrayList<FileServer>(provider.getQuorumFileservers());
        Collections.sort(orderedFileServers, new FileServer.UsageComparator());
        return orderedFileServers.subList(0, getReadQuorumSize());
    }

    public synchronized List<FileServer> getWriteQuorum() {
        List<FileServer> orderedFileServers = new ArrayList<FileServer>(provider.getQuorumFileservers());
        Collections.sort(orderedFileServers, new FileServer.UsageComparator());
        return orderedFileServers.subList(0, getWriteQuorumSize());
    }

    public void setQuorumSizeFix() {
        calculateReadWriteQuorumSizes();
        isQuorumSizeFix = true;
    }

    public synchronized int getReadQuorumSize() {
        if(!isQuorumSizeFix) {
            calculateReadWriteQuorumSizes();
        }
        return readQuorumSize;
    }

    public synchronized int getWriteQuorumSize() {
        if(!isQuorumSizeFix) {
            calculateReadWriteQuorumSizes();
        }
        return writeQuorumSize;
    }

    private void calculateReadWriteQuorumSizes() {
        int size = provider.getQuorumFileservers().size();
        if(size > 0) {
            readQuorumSize = size/2 + 1;
            writeQuorumSize = size/2 + 1;

            assert readQuorumSize + writeQuorumSize > size;
            assert writeQuorumSize > size / 2;
        } else {
            readQuorumSize = 0;
            writeQuorumSize = 0;
        }
    }
}

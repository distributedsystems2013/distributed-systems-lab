package proxy;

public class FileInfo {
    private String filename;
    private FileServer fileserver;
    private int version;

    public FileInfo(String filename, FileServer fileserver, int version) {
        this.filename = filename;
        this.fileserver = fileserver;
        this.version = version;
    }

    public String getFilename() {
        return filename;
    }

    public FileServer getFileserver() {
        return fileserver;
    }

    public int getVersion() {
        return version;
    }

    public boolean fileDoesExist() {
        return version > -1 && fileserver != null;
    }

    @Override
    public String toString() {
        return "FileInfo{" +
                "filename='" + filename + '\'' +
                ", fileserver=" + fileserver +
                ", version=" + version +
                '}';
    }
}

package proxy;

import message.request.*;
import shared.ReflectionSession;
import shared.secured.SecuredReflectionSession;

import java.net.Socket;

public class ProxySession extends SecuredReflectionSession {

    public ProxySession(String ctx, Socket s) {
        super(ctx, s);
        register(new ProxyHandler(this));
    }

    @Override
    protected void onDestroy() {
        if(user != null) {
            UserService.getInstance().logout(user.getName(), this);
            user = null;
        }
    }

    public boolean isLoggedIn() {
        return user != null;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}

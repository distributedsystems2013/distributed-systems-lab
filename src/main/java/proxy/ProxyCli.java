package proxy;

import cli.Command;
import cli.Shell;
import message.Response;
import message.response.FileServerInfoResponse;
import message.response.MessageResponse;
import message.response.UserInfoResponse;
import shared.GenericSocketAcceptor;
import shared.ObjMgr;
import shared.RSAService;
import util.Config;

import java.io.IOException;

public class ProxyCli implements IProxyCli {

    private GenericSocketAcceptor<ProxySession> sa;

    public static void main(String[] args) {
        Shell sh = new Shell("proxycli", System.out, System.in);
        new ProxyCli(new Config("proxy"), sh);
        sh.run();
    }

    public ProxyCli(Config conf, Shell sh) {
        int fileserverListenerPort = conf.getInt("udp.port");
        int fileserverTimeout = conf.getInt("fileserver.timeout");
        int fileserverCheckPeriod = conf.getInt("fileserver.checkPeriod");

        int proxyPort = conf.getInt("tcp.port");

        String ctx = "proxy";
        ObjMgr.getInstance().addService(ctx, "config", conf);
        ObjMgr.getInstance().addService(ctx, "management", ManagementService.getInstance());
        try {
            sh.writeLine("Enter Proxy RSA Key password: ");
            ObjMgr.getInstance().addService(ctx, "rsaService", new RSAService(conf, "proxy", sh.readLine()));
        } catch (IOException e) {
            throw new RuntimeException("can not read password");
        }

        FileserverService.getInstance().startListener(fileserverListenerPort, fileserverTimeout, fileserverCheckPeriod);
        sa = new GenericSocketAcceptor<ProxySession>(ctx, proxyPort, ProxySession.class);
        sa.start();

        UserService.getInstance(); // init userservice

        sh.register(this);
    }

    @Override
    @Command
    public Response fileservers() throws IOException {
        return new FileServerInfoResponse(FileserverService.getInstance().getFileServers());
    }

    @Override
    @Command
    public Response users() throws IOException {
        return new UserInfoResponse(UserService.getInstance().listUsers());
    }

    @Override
    @Command
    public MessageResponse exit() throws IOException {
        FileserverService.getInstance().stopListener();
        ManagementService.getInstance().shutdown();
        sa.interrupt();
        return new MessageResponse("shutdown complete");
    }
}

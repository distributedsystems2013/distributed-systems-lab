package shared;

import java.util.concurrent.ConcurrentHashMap;

public class ObjMgr {

    private static ObjMgr instance = null;
    private ConcurrentHashMap<ContextServiceContainer, Object> objMap = new ConcurrentHashMap<ContextServiceContainer, Object>();

    private ObjMgr() {}

    public static ObjMgr getInstance() {
        if(instance == null)
            instance = new ObjMgr();
        return instance;
    }

    public Object getService(String ctx, String service) {
        return objMap.get(new ContextServiceContainer(ctx, service));
    }

    public void addService(String ctx, String service, Object o) {
        objMap.put(new ContextServiceContainer(ctx, service), o);
    }

    private class ContextServiceContainer {
        private String ctx;
        private String service;

        private ContextServiceContainer(String ctx, String service) {
            this.ctx = ctx;
            this.service = service;
        }

        private String getCtx() {
            return ctx;
        }

        private void setCtx(String ctx) {
            this.ctx = ctx;
        }

        private String getService() {
            return service;
        }

        private void setService(String service) {
            this.service = service;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ContextServiceContainer that = (ContextServiceContainer) o;

            if (!ctx.equals(that.ctx)) return false;
            if (!service.equals(that.service)) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = ctx.hashCode();
            result = 31 * result + service.hashCode();
            return result;
        }
    }
}

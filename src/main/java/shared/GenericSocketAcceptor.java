package shared;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

public class GenericSocketAcceptor<SH extends GenericSession> extends Thread {

    private static final Logger log = Logger.getLogger(GenericSocketAcceptor.class.getName());

    private String ctx;
    private int port;
    private ServerSocket ss;
    private Class<SH> sessionhandler;
    private ExecutorService pool;

    public GenericSocketAcceptor(String ctx, int port, Class<SH> sessionhandler) {
        this.ctx = ctx;
        this.port = port;
        this.sessionhandler = sessionhandler;
        setDaemon(true);
        pool = Executors.newCachedThreadPool();
    }

    @Override
    public void run() {
        try {
            ss = new ServerSocket(port);
            while(!isInterrupted()) {
                Constructor<? extends GenericSession> con = sessionhandler.getConstructor(String.class, Socket.class);
                pool.execute(con.newInstance(ctx, ss.accept()));
            }
        } catch (IOException e) {
            //log.severe("can not open user socket acceptor !");
            interrupt();
        } catch (Exception e) {
            log.severe("false handler given");
            interrupt();
        }
    }

    @Override
    public void interrupt() {
        try {
            if(ss != null && !ss.isClosed())
                ss.close();
            pool.shutdown(); // do not open any new connections
        } catch (IOException e) {
            // just cleanin up
        }
    }
}

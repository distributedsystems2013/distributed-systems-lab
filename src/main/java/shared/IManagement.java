package shared;

import model.CallbackInfo;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.security.PublicKey;
import java.util.Map;

public interface IManagement extends Remote {
    public Integer getReadQuorum() throws RemoteException;

    public Integer getWriteQuorum() throws RemoteException;

    public Map<String, Integer> getTopThreeDownloads() throws RemoteException;

    public PublicKey getProxyPublicKey() throws RemoteException;

    public void setUserPublicKey(String username, PublicKey userPublicKey) throws RemoteException;

    public boolean subscribe(CallbackInfo callbackInfo, ICallbackListener iCallback) throws RemoteException;
}

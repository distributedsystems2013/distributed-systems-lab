package shared;

import org.bouncycastle.openssl.PEMReader;
import org.bouncycastle.openssl.PasswordFinder;
import org.bouncycastle.util.encoders.Base64;
import util.Config;
import util.PKIUtil;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.*;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;

public class RSAService {

    private KeyPair myKey;
    private Config conf;

    public RSAService(Config conf, String name, final String password) {
        try {
            PEMReader in = new PEMReader(new FileReader(conf.getString("keys.dir")+"/"+name+".pem"), new PasswordFinder() {
                @Override
                public char[] getPassword() {
                    return password.toCharArray();
                }
            });
            myKey = (KeyPair) in.readObject();
            in.close();
            this.conf = conf;
        } catch (IOException e) {
            throw new RuntimeException("could not load RSA keypair");
        }
    }

    public String encryptMessage(String receiver, Object o) throws IOException {
        try {
            Cipher crypt = Cipher.getInstance("RSA/NONE/OAEPWithSHA256AndMGF1Padding");
            PublicKey publicKey = PKIUtil.readPublicKey(conf.getString("keys.dir")+ "/" + receiver + ".pub.pem");
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutput out = new ObjectOutputStream(bos);
            out.writeObject(o);
            crypt.init(Cipher.ENCRYPT_MODE, publicKey);
            crypt.update(bos.toByteArray());
            return new String(Base64.encode(crypt.doFinal()));
        } catch (NoSuchAlgorithmException e) {
            throw new IOException("cipher algorithm not found");
        } catch (NoSuchPaddingException e) {
            throw new IOException("cipher algorithm not found");
        } catch (InvalidKeyException e) {
            throw new IOException("invalid key");
        } catch (BadPaddingException e) {
            throw new IOException("bad padding");
        } catch (IllegalBlockSizeException e) {
            throw new IOException("illegal blocksize");
        }
    }

    public Object decryptMessage(String message) throws IOException {
        try {
            Cipher crypt = Cipher.getInstance("RSA/NONE/OAEPWithSHA256AndMGF1Padding");
            crypt.init(Cipher.DECRYPT_MODE, myKey.getPrivate());
            crypt.update(Base64.decode(message));
            ByteArrayInputStream bis = new ByteArrayInputStream(crypt.doFinal());
            ObjectInput ins = new ObjectInputStream(bis);
            return ins.readObject();
        } catch (NoSuchAlgorithmException e) {
            throw new IOException("cipher algorithm not found");
        } catch (NoSuchPaddingException e) {
            throw new IOException("cipher algorithm not found");
        } catch (InvalidKeyException e) {
            throw new IOException("invalid key");
        } catch (BadPaddingException e) {
            throw new IOException("bad padding");
        } catch (IllegalBlockSizeException e) {
            throw new IOException("illegal blocksize");
        } catch (ClassNotFoundException e) {
            throw new IOException("class not found");
        }
    }

}

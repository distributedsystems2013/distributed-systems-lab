package shared;

import message.Request;

import java.lang.annotation.*;

@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Handler {
    Class<? extends Request> handle();
    boolean integrity() default false;
    boolean secured() default false;
}

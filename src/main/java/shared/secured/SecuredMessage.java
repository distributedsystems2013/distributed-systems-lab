package shared.secured;

import message.Request;
import message.Response;

public class SecuredMessage implements Response, Request {

    private String object;

    public SecuredMessage(String object) {
        this.object = object;
    }

    public String getObject() {
        return object;
    }

    @Override
    public String toString() {
        return "SecuredMessage{" +
                "object=" + object +
                '}';
    }
}

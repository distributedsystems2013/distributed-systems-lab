package shared.secured;

import message.Request;
import proxy.User;
import shared.Handler;
import shared.ObjMgr;
import shared.ReflectionSession;
import util.AesSession;
import util.Config;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public class SecuredReflectionSession extends ReflectionSession {

    private static final Logger log = Logger.getLogger(SecuredReflectionSession.class.getName());

    protected Map<Class<? extends Request>, HandlerDefinition> securedReflectionMap = new HashMap<Class<? extends Request>, ReflectionSession.HandlerDefinition>();

    protected User user = null;

    protected AesSession aesSession;
    private String username;

    public SecuredReflectionSession(String ctx, Socket s) {
        super(ctx, s);
        Config config = (Config) ObjMgr.getInstance().getService(ctx, "config");
        registerHandlers(new SecuredHandler(this));
    }

    /// register to right map now
    @Override
    public void registerHandlers(Object obj) {
        for (Method method : obj.getClass().getMethods()) {
            Handler handler = method.getAnnotation(Handler.class);
            if (handler != null && handler.handle() != null) {
                Map<Class<? extends Request>, HandlerDefinition> map;
                if(handler.secured())
                    map = securedReflectionMap;
                else
                    map = reflectionMap;
                if (map.containsKey(handler.handle())) {
                    throw new IllegalArgumentException(String.format("Handler '%s' is already registered.", handler.handle().getName()));
                }
                method.setAccessible(true);
                map.put(handler.handle(), new HandlerDefinition(obj, method));
            }
        }
    }

    protected Map<Class<? extends Request>, HandlerDefinition> getSecuredReflectionMap() {
        return securedReflectionMap;
    }

    public void initAESContext(SecretKey key, byte[] iv) throws IOException {
        this.aesSession = new AesSession(key, iv);
    }

    @Override
    protected void onMessage(Object o) {
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }
}

package shared.secured;

import message.Request;

public class SecLoginReq implements Request {
    private String username;
    private String clientChallange;

    public SecLoginReq(String username, String clientChallange) {
        this.username = username;
        this.clientChallange = clientChallange;
    }

    public String getUsername() {
        return username;
    }

    public String getClientChallange() {
        return clientChallange;
    }
}

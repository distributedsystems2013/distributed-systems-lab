package shared.secured;

import message.Response;

public class SecLoginResp implements Response {
    private String cc;
    private String pc;
    private String sk;
    private String iv;

    public SecLoginResp(String cc, String pc, String sk, String iv) {
        this.cc = cc;
        this.pc = pc;
        this.sk = sk;
        this.iv = iv;
    }

    public String getClientChallenge() {
        return cc;
    }

    public String getProxyChallenge() {
        return pc;
    }

    public String getSecretKey() {
        return sk;
    }

    public String getIv() {
        return iv;
    }
}

package shared.secured;

import message.Request;
import message.response.MessageResponse;
import org.bouncycastle.util.encoders.Base64;
import proxy.User;
import proxy.UserService;
import shared.Handler;
import shared.ObjMgr;
import shared.RSAService;
import shared.ReflectionSession;
import util.Base64Util;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Map;

public class SecuredHandler {

    private SecuredReflectionSession session;

    public SecuredHandler(SecuredReflectionSession session) {
        this.session = session;
    }

    @Handler(handle = SecuredMessage.class)
    public Object secured(SecuredMessage message) throws IOException {
        if(session.aesSession == null || session.getUsername() == null)
            return new MessageResponse("not logged in");
        Object resp = null;
        try {
            Object o =  session.aesSession.stringToObject(message.getObject());
            if(session.user == null && !(o instanceof AesSyn)) {
                session.cleanup();
                throw new IOException("syn missing, kicking out");
            }
            Map<Class<? extends Request>, ReflectionSession.HandlerDefinition> reflectionMap = session.getSecuredReflectionMap();
            SecuredReflectionSession.HandlerDefinition handlerDefinition = reflectionMap.get(o.getClass());
            if(handlerDefinition.targetMethod.getParameterTypes().length > 0) // our methods require either the request DTO or nothing
                resp = handlerDefinition.targetMethod.invoke(handlerDefinition.targetObject, o);
            else
                resp = handlerDefinition.targetMethod.invoke(handlerDefinition.targetObject);
        } catch (IllegalAccessException e) {
            throw new IOException("no handler registered", e);
        } catch (InvocationTargetException e) {
            throw new IOException("no handler registered", e);
        }
        return new SecuredMessage(session.aesSession.objectToString(resp));
    }


    /**
     * the only RSA Message that is received is the first login request
     * @param req login reqeust
     * @return proxy challenge
     */
    @Handler(handle = RSAMessage.class)
    public RSAMessage loginRequest(RSAMessage req) throws IOException {
        try {
            RSAService rsaService = ((RSAService) ObjMgr.getInstance().getService(session.getCtx(), "rsaService"));
            Object o = rsaService.decryptMessage(req.getEncryptedMessage());
            if(!(o instanceof SecLoginReq))
                throw new IOException("received unexpected message");
            SecLoginReq slreq = (SecLoginReq) o;
            String username = slreq.getUsername();
            String clientChallenge = slreq.getClientChallange();

            SecureRandom secureRandom = new SecureRandom();

            byte[] proxyChallengeByte = new byte[32];
            secureRandom.nextBytes(proxyChallengeByte);
            String proxyChallenge = new String(Base64.encode(proxyChallengeByte));

            String secretKey = null;
            SecretKey secretKeyObject = null;
            try {
                KeyGenerator generator = KeyGenerator.getInstance("AES");
                generator.init(256);
                secretKeyObject = generator.generateKey();
                secretKey = Base64Util.objectToString(secretKeyObject);
            } catch (NoSuchAlgorithmException e) {
                throw new IOException(e);
            }

            byte[] ivByte = new byte[16];
            secureRandom.nextBytes(ivByte);
            String iv = new String(Base64.encode(ivByte));

            session.setUsername(username);

            session.initAESContext(secretKeyObject, Base64.decode(iv));

            SecLoginResp slresp = new SecLoginResp(clientChallenge, proxyChallenge, secretKey, iv);
            return new RSAMessage(rsaService.encryptMessage(username, slresp));
        } catch (IOException e) {
            throw new IOException("login failed, exception thrown", e);
        }
    }

    @Handler(handle = AesSyn.class, secured = true)
    public AesAck aesAck() {
        User login = UserService.getInstance().login(session.getUsername(), session);
        session.setUser(login);
        return new AesAck();
    }
}

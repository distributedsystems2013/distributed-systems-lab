package shared.secured;

import message.Request;

public class AesSyn implements Request {
    private String proxyChallenge;

    public AesSyn(String proxyChallenge) {
        this.proxyChallenge = proxyChallenge;
    }

    public String getProxyChallenge() {
        return proxyChallenge;
    }
}

package shared.secured;

import message.Request;
import message.Response;

/**
 * Container for encrypted messages, en/decryption relays on handler
 */
public class RSAMessage implements Request, Response{

    private String encryptedMessage;

    public RSAMessage(String encryptedMessage) {
        this.encryptedMessage = encryptedMessage;
    }

    public String getEncryptedMessage() {
        return encryptedMessage;
    }
}

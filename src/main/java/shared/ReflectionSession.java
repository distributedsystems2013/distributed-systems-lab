package shared;

import cli.Command;
import message.Request;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Method;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public class ReflectionSession extends GenericSession {

    private static final Logger log = Logger.getLogger( ReflectionSession.class.getName() );

    protected Map<Class<? extends Request>, HandlerDefinition> reflectionMap = new HashMap<Class<? extends Request>, HandlerDefinition>();
    protected boolean stopped = false;

    public ReflectionSession(String ctx, Socket s) {
        super(ctx, s);
    }

    public void register(Object obj) {
        registerHandlers(obj);
        // register atExit, and socket setter
    }

    public void registerHandlers(Object obj) {
        for (Method method : obj.getClass().getMethods()) {
            Handler handler = method.getAnnotation(Handler.class);
            if (handler != null && handler.handle() != null) {
                if (reflectionMap.containsKey(handler.handle())) {
                    throw new IllegalArgumentException(String.format("Handler '%s' is already registered.", handler.handle().getName()));
                }
                method.setAccessible(true);
                reflectionMap.put(handler.handle(), new HandlerDefinition(obj, method));
            }
        }
    }

    public void cleanup() {
        super.cleanup();
        onDestroy();
        stopped=true;
    }

    @Override
    public void run() {
        onCreate();
        while(!stopped) {
            try {
                ObjectInputStream ins = new ObjectInputStream(socket.getInputStream());
                Object o = ins.readObject();
                onMessage(o);
                HandlerDefinition handlerDefinition = reflectionMap.get(o.getClass());
                Object resp;
                if(handlerDefinition.targetMethod.getParameterTypes().length > 0) // our methods require either the request DTO or nothing
                    resp = handlerDefinition.targetMethod.invoke(handlerDefinition.targetObject, o);
                else
                    resp = handlerDefinition.targetMethod.invoke(handlerDefinition.targetObject);
                ObjectOutputStream ous = new ObjectOutputStream(socket.getOutputStream());
                ous.writeObject(resp);
            } catch (IOException e){
                log.info("socket closed!");
                cleanup();
                return;
            } catch (Exception e) {
                log.info("got invalid response!");
                // disconnect client ?!
            }
        }
    }

    protected void onCreate() {}
    protected void onDestroy() {}
    protected void onMessage(Object o) {}


    public class HandlerDefinition {
        public Object targetObject;
        public Method targetMethod;

        public HandlerDefinition(Object targetObject, Method targetMethod) {
            this.targetObject = targetObject;
            this.targetMethod = targetMethod;
        }
    }

}

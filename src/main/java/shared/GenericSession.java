package shared;

import java.io.IOException;
import java.net.Socket;

public abstract class GenericSession implements Runnable {
    protected final Socket socket;
    protected final String ctx;

    public GenericSession(String ctx, Socket s) {
        this.ctx = ctx;
        this.socket = s;
    }

    public String getCtx() {
        return ctx;
    }

    public void cleanup() {
        if(socket != null && !socket.isClosed())
            try {
                socket.close();
            } catch (IOException e) {
                // doesnt matter
            }
    }
}

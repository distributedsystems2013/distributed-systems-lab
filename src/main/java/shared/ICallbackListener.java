package shared;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ICallbackListener extends Remote {
    public void downloadCountReached(String message) throws RemoteException;
}

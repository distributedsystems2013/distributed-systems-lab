package shared;

import java.io.*;
import java.util.*;

public class StorageService {

    private File storagePath;
    private Map<String, Integer> versionInfo = new HashMap<String, Integer>();

    public StorageService() {}

    public void setStoragePath(String path) throws IOException {
        storagePath = new File(path);
        if(!storagePath.isDirectory()) {
            storagePath = null;
            throw new IOException("path is not a directory!");
        }
    }

    public Set<String> listFiles() throws IOException {
        if(storagePath == null)
            throw new IOException("storage not set!");

        Set<String> fileNames = new HashSet<String>();
        for(File f : storagePath.listFiles()) {
            if(f.isFile())
                fileNames.add(f.getName());
        }
        return fileNames;
    }

    public File getFileByName(String filename) {
        return new File(storagePath.getAbsolutePath()+"/"+filename);
    }

    public boolean exists(String fileName) {
        return getFileByName(fileName) != null && getFileByName(fileName).exists();
    }

    public long size(String filename) {
        return exists(filename)? getFileByName(filename).length() : -1;
    }

    public int version(String filename) {
        if(!exists(filename)) {
            return -1;
        }
        Integer version = versionInfo.get(filename);
        if(version == null) {
            version = 0;
            versionInfo.put(filename, version);
        }
        return version;
    }

    public void deleteFile(String filename) {
        if(exists(filename))
            getFileByName(filename).delete();
    }

    public void writeFile(String filename, int version, byte[] content) throws IOException {
        FileOutputStream fos = new FileOutputStream(getFileByName(filename));
        fos.write(content);
        fos.close();
        versionInfo.put(filename, version);
    }

    public byte[] readFile(String filename) throws IOException {
        FileInputStream fis = new FileInputStream(getFileByName(filename));
        byte[] content = new byte[fis.available()];
        fis.read(content);
        fis.close();
        return content;
    }

}

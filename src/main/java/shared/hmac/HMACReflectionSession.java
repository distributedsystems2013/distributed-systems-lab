package shared.hmac;

import message.Request;
import org.bouncycastle.util.encoders.Hex;
import shared.Handler;
import shared.ObjMgr;
import shared.ReflectionSession;
import shared.StorageService;
import util.Config;

import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.lang.reflect.Method;
import java.net.Socket;
import java.security.Key;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public class HMACReflectionSession extends ReflectionSession {

    private static final Logger log = Logger.getLogger( HMACReflectionSession.class.getName() );

    protected Map<Class<? extends Request>, HandlerDefinition> hMACReflectionMap = new HashMap<Class<? extends Request>, HandlerDefinition>();

    protected HMACSession hmacSession;

    public HMACReflectionSession(String ctx, Socket s) {
        super(ctx, s);
        Config config = (Config) ObjMgr.getInstance().getService(ctx, "config");
        try {
            String keyPath = config.getString("hmac.key");
            hmacSession = new HMACSession(keyPath);
            registerHandlers(new HMACHandler(this));
        } catch (FileNotFoundException e) {
            throw new RuntimeException("no hmac provided!");
        } catch (IOException e) {
            throw new RuntimeException("no hmac provided!");
        }
    }

    /// register to right map now
    @Override
    public void registerHandlers(Object obj) {
        for (Method method : obj.getClass().getMethods()) {
            Handler handler = method.getAnnotation(Handler.class);
            if (handler != null && handler.handle() != null) {
                Map<Class<? extends Request>, HandlerDefinition> map;
                if(handler.integrity())
                    map = hMACReflectionMap;
                else
                    map = reflectionMap;
                if (map.containsKey(handler.handle())) {
                    throw new IllegalArgumentException(String.format("Handler '%s' is already registered.", handler.handle().getName()));
                }
                method.setAccessible(true);
                map.put(handler.handle(), new HandlerDefinition(obj, method));
            }
        }
    }

    public HMACSession getHmacSession() {
        return hmacSession;
    }

    protected Map<Class<? extends Request>, HandlerDefinition> gethMACReflectionMap() {
        return hMACReflectionMap;
    }

    @Override
    protected void onMessage(Object o) {
        if(!(o instanceof HMACMessage))
            System.out.println("got non verified message: " + o);
    }
}

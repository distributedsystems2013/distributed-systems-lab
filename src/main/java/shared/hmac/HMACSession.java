package shared.hmac;

import org.bouncycastle.util.encoders.Base64;
import org.bouncycastle.util.encoders.Hex;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.security.*;

public class HMACSession {

    private Mac hMac;

    public HMACSession(String keyPath) throws IOException {
        try {
            FileInputStream fis = new FileInputStream(keyPath);
            byte[] keyBytes = new byte[1024];
            fis.read(keyBytes);
            fis.close();
            byte[] keyHex = Hex.decode(keyBytes);
            Key key = new SecretKeySpec(keyHex, "HmacSHA256");
            hMac = Mac.getInstance("HmacSHA256");
            hMac.init(key);
        } catch (NoSuchAlgorithmException e) {
            // algorithm hardcoded, will never happen
        } catch (InvalidKeyException e) {
            throw new InvalidParameterException("invalid key");
        }
    }

    public HMACSession(Key key) {
        try {
            hMac = Mac.getInstance("HmacSHA256");
            hMac.init(key);
        } catch (NoSuchAlgorithmException e) {
            // algorithm hardcoded, will never happen
        } catch (InvalidKeyException e) {
            throw new InvalidParameterException("invalid key");
        }
    }

    public HMACMessage buildMessage(Object o) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutput out = new ObjectOutputStream(bos);
        out.writeObject(o);
        hMac.update(bos.toByteArray());
        byte[] hmac = hMac.doFinal();
        return new HMACMessage(new String(Base64.encode(hmac)), o);
    }

    public Object verifyMessage(HMACMessage o) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutput out = new ObjectOutputStream(bos);
        out.writeObject(o.getObject());
        hMac.update(bos.toByteArray());
        byte[] hmac = hMac.doFinal();
        byte[] mhmac = Base64.decode(o.getHmac().getBytes());
        if(!MessageDigest.isEqual(hmac, mhmac))
            throw new IOException("message corrupted");

        return o.getObject();
    }

}

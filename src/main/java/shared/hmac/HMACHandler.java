package shared.hmac;

import message.Request;
import shared.Handler;
import shared.ReflectionSession;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

public class HMACHandler {

    private HMACReflectionSession session;

    public HMACHandler(HMACReflectionSession session) {
        this.session = session;
    }

    @Handler(handle = HMACMessage.class)
    public Object hmac(HMACMessage message) throws IOException {
       Object resp = null;
        try {
            Object o = null;
            try {
                o =  session.getHmacSession().verifyMessage(message); /* throws IOException if message corruption */
            } catch(IOException e) {
                System.out.println(message);
                return new TamperedMessageResponse();
            }
            Map<Class<? extends Request>, ReflectionSession.HandlerDefinition> reflectionMap = session.gethMACReflectionMap();
            HMACReflectionSession.HandlerDefinition handlerDefinition = reflectionMap.get(o.getClass());
            if(handlerDefinition.targetMethod.getParameterTypes().length > 0) // our methods require either the request DTO or nothing
                resp = handlerDefinition.targetMethod.invoke(handlerDefinition.targetObject, o);
            else
                resp = handlerDefinition.targetMethod.invoke(handlerDefinition.targetObject);
        } catch (IllegalAccessException e) {
            throw new IOException("no handler registered");
        } catch (InvocationTargetException e) {
            throw new IOException("no handler registered");
        }
        return session.getHmacSession().buildMessage(resp);
    }
}

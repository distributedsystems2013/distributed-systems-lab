package shared.hmac;

import message.Request;
import message.Response;

public class HMACMessage implements Request, Response {

    private String hmac;
    private Object object;

    public HMACMessage(String hmac, Object object) {
        this.hmac = hmac;
        this.object = object;
    }

    public String getHmac() {
        return hmac;
    }

    public Object getObject() {
        return object;
    }

    @Override
    public String toString() {
        return "HMACMessage{" +
                "hmac='" + hmac + '\'' +
                ", object=" + object +
                '}';
    }
}


# Evaluation

This file provides the machine characteristics and key findings during testing.


## Machine characteristics

* Operating system version: Windows 7 Home Premium, SP1, 64 bit
* CPU: Intel Core i5-2430M
* Number of CPUs: 2
* Clock frequency of CPUs: 2.40 GHz, 2.40 GHz
* RAM capacity: 8 GB


## Key findings

_What were the results during the test and which parameters were chosen._

The system is very stable. There are no system limits then machine limits.
With a high number of clients with lots of up- and downloads, the only notable
issues are timeouts. The reason for this timeouts is that the
server (mainly the proxy) is not able to respond fast enough to the client
requests, so a timeout on the client side happens. The limit was the machine,
which had to handle so many threads in parallel that the test was no more realistic,
because in reality, every client, proxy and file server is hosted on a different
machine and not on one single machine.

### Limit

_at least the limit values for all configuration parameters_

The following parameters are constant:
fileSizeKB=2
overwriteRatio=0.5

With only few clients, a mediate up- and download load is possible to be processed by
our system:
  clients=3
  uploadsPerMin=20
  downloadsPerMin=100

Another parameter test:
  clients=3
  uploadsPerMin=200
  downloadsPerMin=500

upload: 169, overwrite: 186, download: 500, timeouts: 0 = ~850 requests in one minute
This shows us that this amount of requests (here 2100 requests per minute) can not be processed by our system

Further tests show this limit:
  clients=3
  uploadsPerMin=500
  downloadsPerMin=1000

The system was only able to handle about 2350 requests (upload: 430, overwrite: 422, download: 1504, timeouts: 1)
in 3 minutes, which is about 750 to 800 requests per minute. This shows that 1500 requests from 3 clients are not
possible to be processed.

We tested it with some more clients:
  clients=30
  uploadsPerMin=2
  downloadsPerMin=15

upload: 17, overwrite: 9, download: 862, timeouts: 60, in two minutes: ~450 requests per minute
The system worked quite good. Some timeouts but acceptable.

But to be sure, that this is a server side limit, we tested it with many clients:
  clients=100
  uploadsPerMin=2
  downloadsPerMin=10

upload: 26, overwrite: 98, download: 333, timeouts: 1384:
The test went quite okay, but the main problem here was, that the proxy got some timeouts from the fileservers
and so the clients also got timeouts for their request.

And we also test it with a huge amount of clients:
  clients=1000
  uploadsPerMin=2
  downloadsPerMin=10

upload: 1, overwrite: 498, download: 0, timeouts: 4201
The system limits were reaches. The system nearly froze and only slowly reacted. Maybe it would be better to have
our clients and servers on independend machines, to create more realistic tests results.

We also did long running tests:
for example:
  clients=20
  uploadsPerMin=2
  downloadsPerMin=15
upload: 167, overwrite: 163, download: 3081, timeouts: 15
in 10 minutes with a very good performance and no crashes.

We can summarize that our machines limit where at ~800 requests from ~5 clients per minute, or at ~450 requests from
~35 clients per minute.

